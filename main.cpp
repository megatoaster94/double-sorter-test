#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <string>
#include <random>
#include <algorithm>
#include <ctime>
#include <chrono>
#include <iomanip>

#define USAGE usage(argv[0])

const uint NUMBERS_AMOUNT = 134217728; //amount of numbers in 1GB
const uint MAX_FILE_SIZE = 1024*1024*1024;
const uint NUMBERS_AMOUNT_100MB = 13107200; //amount of numbers in 100MB
//const uint NUMBER_AMOUNT_100MB = 100;
const uint TMP_FILE_SIZE = 1024*1024*1024/20;


class data_t
{
public:
    data_t()
    {

    }

    data_t(std::vector<std::string> files) :
        _files(files)
    {
        for (std::string f : _files)
        {
            std::ifstream stream(f);
            _streams.push_back(std::move(stream));
            double str;
            stream >> str;
            _data.push_back(str);
        }
    }

    bool get_min(double *number)
    {
        if (_streams.size() == 0)
        {
            return false;
        }
        std::vector<double>::iterator itr = std::min_element(_data.begin(), _data.end());
        size_t ind = std::distance(_data.begin(), itr);
        *number = _data.at(ind);
        double num;
        _streams.at(ind) >> num;
        _data.at(ind) = num;
        if (_streams.at(ind).eof())
        {
            _streams.erase(_streams.begin() + ind);
            _data.erase(itr);
            std::remove(_files.at(ind).c_str());
            _files.erase(_files.begin() + ind);
        }

        return true;
    }

private:
    std::vector<std::string> _files;
    std::vector<std::ifstream> _streams;
    std::vector<double> _data;
};


void usage(char *program)
{
    std::cout << "Usage: " << program << " [--create] infile outfile - for sort infile into outfile" << std::endl;
    std::cout << "\tinfile: the input file" << std::endl;
    std::cout << "\toutfile: the output file" << std::endl;
    std::cout << "\t--create: for make infile" << std::endl;
}

double random_double()
{
    std::random_device rd;
    std::default_random_engine re(rd());
    std::uniform_real_distribution<double> unif(0, std::numeric_limits<double>::max());
    return unif(re);
}

void timer(std::chrono::time_point<std::chrono::high_resolution_clock> start, std::string message)
{
    const auto finish = std::chrono::high_resolution_clock::now();
    auto duration = finish - start;
    const auto min = std::chrono::duration_cast< std::chrono::minutes > ( duration );
    duration -= min;
    const auto sec = std::chrono::duration_cast< std::chrono::seconds > ( duration );
    duration -= sec;
    const auto milli = std::chrono::duration_cast< std::chrono::milliseconds > ( duration );
    std::cout << message
              << min.count() << " m "
              << sec.count() << "."
              << std::setw( 3 ) << std::setfill( '0' ) << milli.count() << " s\n";
}

int main(int argc, char *argv[])
{
    const auto start = std::chrono::high_resolution_clock::now();

    if (argc == 3 || argc == 4)
    {
        if (!strcmp(argv[1], "--create"))
        {
            std::ofstream generated_infile(argv[2]);
            /*for (size_t i = 0; i < NUMBER_AMOUNT; ++i)
            {
                generated_infile << random_double() << std::endl;
            }*/
            std::random_device rd;
            std::default_random_engine re(rd());
            std::uniform_real_distribution<double> unif(0, std::numeric_limits<double>::max());

            std::fstream::pos_type size = 0;
            while (size < MAX_FILE_SIZE/*10000*/)
            {
                std::ostringstream str;
                str << unif(re);

                const auto text = str.str() + '\n';

                generated_infile << text;

                size += ( text.length() + 1 );
            }
            generated_infile.close();

            timer(start, "Elapsed time for file creation: ");
        }
        else if (argc != 3)
        {
            USAGE;
            return 1;
        }

        std::ifstream infile(argv[argc == 4 ? 2 : 1]);
        if (infile.is_open())
        {
            //divide numbers in infile to tmp files
            std::vector<std::string> tmp_files;
            while (!infile.eof())
            {
                std::string tmp_filename = std::tmpnam(nullptr);
                std::cout << "Temporary file: " << tmp_filename << std::endl;
                std::ofstream tmp_outfile(tmp_filename);
                for (size_t i = 0; i < NUMBERS_AMOUNT_100MB/*100*//*/2*/ && !infile.eof(); ++i)
                {
                    std::string tmp_str;
                    infile >> tmp_str;
                    tmp_outfile << tmp_str << /*std::endl*/'\n';
                }
                tmp_files.push_back(tmp_filename);
            }
            infile.close();

            //sort numbers in tmp files
            std::vector<double> numbers;
            numbers.reserve(NUMBERS_AMOUNT_100MB/*/2*/);
            for (std::string tmp_file : tmp_files)
            {
                numbers.clear();
                std::ifstream tmp_infile(tmp_file);
                while (!tmp_infile.eof())
                {
                    double num;
                    tmp_infile >> num;
                    numbers.push_back(num);
                }
                tmp_infile.close();

                std::sort(numbers.begin(), numbers.end());

                std::ofstream tmp_outfile(tmp_file, std::ofstream::trunc);
                for (double n : numbers)
                {
                    tmp_outfile << n << /*std::endl*/'\n';
                }
                tmp_outfile.close();
            }



            data_t data(tmp_files);

            std::ofstream outfile(argv[argc == 4 ? 3 : 2], std::ofstream::trunc);
            double num;
            while (data.get_min(&num))
            {
                outfile << num << '\n';
            }
            outfile.close();
        }
        else
        {
            std::cout << "Error opening file " << argv[2] << "!" << std::endl;
            return 1;
        }
    }
    else
    {
        USAGE;
        return 1;
    }

    timer(start, "Elapsed time: ");

    return 0;
}
